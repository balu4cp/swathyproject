# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User

# Create your models here.

class Feedback(models.Model):
	user_data=models.ForeignKey(User)
	how_you_rate_the_website_overall=models.CharField(max_length=50,choices=(("excellent","excellent"),("good","good"),("satisfactory","satisfactory")),default="excellent")
	suggestions=models.TextField(max_length=2000)
	
