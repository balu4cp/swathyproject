from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm,UserChangeForm
from Userapp.models import Feedback


class RegisterForm(UserCreationForm):
	class Meta:
		model=User
		fields=['username','email','password1','password2']
class EditUserForm(UserChangeForm):
	UserChangeForm.password = None
	class Meta:
		model=User
		fields=['username','email']

class FeedbackForm(forms.ModelForm):
	class Meta:
		model=Feedback
		fields=['user_data','how_you_rate_the_website_overall','suggestions']
		how_you_rate_the_website_overall=forms.ChoiceField(label="",initial='',widget=forms.Select(),required=True)
	



