# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,redirect

from django.views.generic import View,TemplateView,CreateView,UpdateView,ListView
from django.contrib.auth.forms import AuthenticationForm
from Userapp.forms import RegisterForm,EditUserForm,FeedbackForm
from django.contrib.auth import authenticate,login
from django.contrib.auth.models import User
from Userapp.models import Feedback
from django.conf import settings
import urllib
import urllib2
import json



from django.core.mail import send_mail

# Create your views here.

class Login(View):
	def get(self,request):
		form=AuthenticationForm()
		context={'form':form}
		if not request.user.is_authenticated:
			return render(request,'login.html',context)
		else:
			return redirect('home')
	def post(self,request):
		a=request.POST.get('username')
		b=request.POST.get('password')
		user=authenticate(username=a,password=b)
		
		''' 
		recaptcha_response = request.POST.get('g-recaptcha-response')
		url = 'https://www.google.com/recaptcha/api/siteverify'
		values = {
		'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
		'response': recaptcha_response
		    }
		data = urllib.urlencode(values)
		req = urllib2.Request(url, data)
		response = urllib2.urlopen(req)
		result = json.load(response)
		 '''
		if user:# and result['success']:
			login(request,user)
			if request.user.is_superuser:
				return redirect('adminpage')
			else:
				return redirect('home')
		else:
			form=AuthenticationForm
			context={'form':form}
			return render(request,'login.html',context)
	
class Home(TemplateView):
	template_name='home.html'
	
class Register(CreateView):
	form_class=RegisterForm
	template_name='register.html'
	success_url='/home'
	def post(self,request):
		send_mail('registration','successful','allrecipe@example.com',[request.POST.get('username')+'@example.com'])
		return super(Register,self).get(self,request)

	



class UserPage(TemplateView):
	template_name='userpage.html'
class ViewProfile(TemplateView):
	template_name='profile.html'
	def get(self,request):
		print(request.user)
		return render(request,self.template_name,{'user':request.user})
class AdminView(TemplateView):
	template_name='adminpage.html'
class EditProfile(UpdateView):
	form_class=EditUserForm
	model=User
	template_name='editprofile.html'
	def get_object(self, queryset=None):
		return self.request.user
	def get_success_url(self):
		return '/profile'

class Feedbackv(CreateView):
	form_class=FeedbackForm
	model=Feedback
	template_name='feedback.html'
	success_url='feedback'

class ViewFeedback(TemplateView):
	template_name='viewfeedback.html'
	
	model=Feedback
	def get(self,request):	
		x=Feedback.objects.all()
		print(x)
		return render(request,self.template_name,{'feedback':x})









	
