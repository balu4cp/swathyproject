from django.conf.urls import url,include
from django.contrib.auth import views as auth_views
#from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from Userapp.views import Login,Home,Register,UserPage,ViewProfile,AdminView,EditProfile,Feedbackv,ViewFeedback

urlpatterns=[
	url(r'^login/',Login.as_view(),name='login'),
	url(r'^logout/',auth_views.logout,name='logout'),
	url(r'^home/',Home.as_view(),name='home'),
	url(r'^register/',Register.as_view(),name='register'),
	url(r'^userpage/',UserPage.as_view(),name='userpage'),
	url(r'^profile/',ViewProfile.as_view(),name='Profile'),
	url(r'^editprofile/',EditProfile.as_view(),name='editprofile'),
	url(r'^adminpage/',AdminView.as_view(),name='adminpage'),
	url(r'^feedback/',Feedbackv.as_view(),name='feedback'),
	url(r'^viewfeedback/',ViewFeedback.as_view(),name='viewfeedback'),
	
	url('^', include('django.contrib.auth.urls')),

]

#urlpatterns += staticfiles_urlpatterns 
