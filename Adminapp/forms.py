from django import forms
from Adminapp.models import Category,SubCategory,Recipe

class CategoryForm(forms.ModelForm):
	class Meta:
		model=Category
		fields=['category_name','category_pic']
		
class SubCategoryForm(CategoryForm):
	class Meta:
		model=SubCategory
		fields=['category_name','subcategory_name','subcategory_pic']
		

class RecipeForm(SubCategoryForm):
	class Meta:
		model=Recipe
		fields=['category_name','subcategory_name','recipe_name','recipe_pic','ingrediants','instructions','price']
	def __init__(self,*args, **kwargs):
			super(RecipeForm,self).__init__(*args, **kwargs)
			self.fields['subcategory_name'].queryset=SubCategory.objects.none()


			if 'category_name' in self.data:
				try:
					category_name=int(self.data.get('category_name'))
					self.fields['subcategory_name'].queryset=SubCategory.objects.filter(category_name=Category.objects.get(pk=category_name)).order_by('subcategory_name')

				except (ValueError, TypeError):
					pass
			elif self.instance.pk:
				self.fields['subcategory_name'].queryset = self.instance.Category.subcategory_set.order_by("subcategory_name")






class RecipeDetails(forms.ModelForm):
	class Meta:
		model=Recipe
		fields=['recipe_name','recipe_pic','ingrediants','instructions','price']

class RecipeData(forms.ModelForm):
	class Meta:
		model=Recipe
		fields=['recipe_name','recipe_pic','ingrediants','instructions','approved']

