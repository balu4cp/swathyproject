from django.conf.urls import url,include

from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from Adminapp.views import AddCategory,AddSubCategory,AddRecipe,DownloadView,ViewRecipe,EditRecipe,fullrecipe,subcatlist,search,ViewRecipeus,EditRecipeus
urlpatterns=[
	url(r'^addcat/',AddCategory.as_view(),name='addcat'),
	url(r'^addsubcat/',AddSubCategory.as_view(),name='addsubcat'),
	url(r'^addrecipe/',AddRecipe.as_view(),name='addrecipe'),
	url(r'^viewrecipe/',ViewRecipe.as_view(),name='viewrecipe'),
	url(r'^editrecipe/(?P<id>[0-9]+)/$',EditRecipe.as_view(),name='editrecipe'),
	url(r'^ajax/subcatlist/',subcatlist,name='ajaxsubcatlist'),
	url(r'^searchrecipe/',search,name='searchrecipe'), 
	url(r'^viewrecipeus',ViewRecipeus.as_view(),name='viewrecipeus'),
	url(r'^editrecipeus/(?P<id>[0-9]+)/$',EditRecipeus.as_view(),name='editrecipeus'),
	url(r'^fullrecipe/',fullrecipe,name='fullrecipe'),
	#url(r'^payment/',Payment.as_view(),name='payment'),
	url(r'^download/',DownloadView.as_view(),name='download'),
	
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

