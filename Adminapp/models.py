# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Category(models.Model):
	category_name=models.CharField(max_length=255)
	category_pic=models.ImageField(upload_to="profilepic")
	def __str__(self):
		return self.category_name
class SubCategory(models.Model):
	category_name=models.ForeignKey(Category,on_delete=models.CASCADE)	
	subcategory_name=models.CharField(max_length=255)
	subcategory_pic=models.ImageField(upload_to="profilepic")
	def __str__(self):
		return self.subcategory_name
class Recipe(models.Model):
	category_name=models.ForeignKey(Category)
	subcategory_name=models.ForeignKey(SubCategory)
	recipe_name=models.CharField(max_length=255)
	recipe_pic=models.ImageField(upload_to="profilepic")
	ingrediants=models.TextField(max_length=2000)
	instructions=models.TextField(max_length=2000)
	price=models.IntegerField()
	approved=models.BooleanField(default=False)
	def __str__(self):
		return self.recipe_name


	
