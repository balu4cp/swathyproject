# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from Adminapp.forms import CategoryForm,SubCategoryForm,RecipeForm,RecipeDetails,RecipeData
from django.views.generic import CreateView,ListView,UpdateView,TemplateView
from Adminapp.models import Category,SubCategory,Recipe
#from django.http import HttpResponse
#from django.template.loader import render_to_string
#from weasyprint import HTML
#import tempfile

# Create your views here.

class AddCategory(CreateView):
	form_class=CategoryForm
	model=Category
	template_name='adminpage.html'
	success_url='addcat'

class AddSubCategory(CreateView):
	form_class=SubCategoryForm
	model=SubCategory
	template_name='adminpage.html'
	success_url='addsubcat'

def subcatlist(request):
	category_id=int(request.GET.get('category'))
	print(category_id)
	subcategorylist=SubCategory.objects.filter(category_name=Category.objects.get(pk=category_id)).order_by('subcategory_name')
	print(subcategorylist)
	return render(request,'subcategory.html',{'subcategorylist': subcategorylist})
	
		

class AddRecipe(CreateView):
	form_class=RecipeForm
	model=Recipe
	template_name='addrecipe.html'
	success_url='addrecipe'

class ViewRecipe(ListView):
	template_name='viewrecipe.html'
	context_object_name='recipe'
	model=Recipe
	def get_queryset(self):
		x = super(ViewRecipe,self).get_queryset()
		x = x.filter(approved=True)
		return x



class EditRecipe(UpdateView):
	template_name='editrecipe.html'
	model=Recipe
	form_class=RecipeDetails
	slug_field='id'
	slug_url_kwarg='id'
	def get_success_url(self):
		return '/viewrecipe/'

def search(request):
	if request.method =='POST':
		recipe_name=request.POST.get('search')
		status=Recipe.objects.filter(recipe_name__contains=recipe_name)
		if status==None:
			pass
		else:
			return render(request,'search.html',{'recipe_name':status})

class ViewRecipeus(ListView):
	template_name='viewrecipeus.html'
	context_object_name='recipe'
	model=Recipe

	
class EditRecipeus(UpdateView):
	template_name='editrecipeus.html'
	model=Recipe
	form_class=RecipeData
	slug_field='id'
	slug_url_kwarg='id'
	def get_success_url(self):
		return '/viewrecipeus/'
def fullrecipe(request):
	recipelist=Recipe.objects.all()
	return render(request,'fullrecipe.html',{'recipelist':recipelist})
'''
class Payment(View):
	
		def get(self,request):
		    p=request.session['recipe_name']
		    z=int(p)
		    ob1=Recipe.objects.get(pk=z)
		    print(ob1)
		    
		    html_string = render_to_string('viewrecipeus.html', {'o1':ob1})
		    html = HTML(string=html_string)
		    result = html.write_pdf()

		    #Creating http response
		    response = HttpResponse(content_type='application/pdf')
		    response['Content-Disposition'] = 'inline; filename=payment.pdf'
		    response['Content-Transfer-Encoding'] = 'binary'
		    with tempfile.NamedTemporaryFile(delete=True) as output:
			output.write(result)
			output.flush()
			output = open(output.name, 'r')
			response.write(output.read())
		    return response
'''
class DownloadView(TemplateView):
	template_name='download.html'
	#def get(self,request):
		#recipe_name=request.GET.get('id')
		#print("================")
		#print(recipe_name)
		#print(request.GET)
		#request.session['recipe_name']=recipe_name
		#return super(DownloadView,self).get(self,request)



		
	


	
			
	
		
 





